#+title: Org Mode Wiki example

This repo is an example of how you can build and publish a wiki with [[https://orgmode.org][Org Mode]].

Thanks to [[https://systemcrafters.net/publishing-websites-with-org-mode/building-the-site/][SystemCrafters]] and this great post on their website:
https://systemcrafters.net/publishing-websites-with-org-mode/building-the-site/ and [[https://gitlab.com/OlMon][OlMon]] for this
repository full of themes: https://olmon.gitlab.io/org-themes/


* How to build?
:PROPERTIES:
:ID:       d7c1bbef-421e-40f6-b3ac-4d52534711ea
:END:
First, make ~build.sh~ an executable:
#+begin_src bash
chmod +x build.sh
#+end_src

Now, run ~./build.sh~:
#+begin_src bash
./build.sh
#+end_src

* To generate the menu
:PROPERTIES:
:ID:       f990c066-4c00-41df-bfe0-01764562ec1a
:END:
Go to ./content/menu.org and run the code block. On the "RESULTS" section, remove the ~:~ at the begining
of the result.

* Deploy
:PROPERTIES:
:ID:       2986441f-1d14-4dcd-9282-9c7aa194a6e8
:END:
This package is self deployable, ready to run behind a proxy (like NGINX Proxy Manager)

Make sure you have Emacs installed on the destination server.

First, make ~deploy.sh~ an executable:
#+begin_src bash
chmod +x deploy.sh
#+end_src

Now run ~./deploy.sh~:
#+begin_src bash
NAME="org-wiki" PORT=8080 ./deploy.sh
#+end_src


* License
:PROPERTIES:
:ID:       847b2c69-f711-4bde-b75c-c95a8d9cba9e
:END:
This project is licensed under the GNU General Public License, version 3.0. For more details, see
[[https://gitlab.com/rogs/org-web-wiki-template/-/blob/master/LICENSE][LICENSE]].
