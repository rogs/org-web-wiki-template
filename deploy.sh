#!/usr/bin/env sh
docker rm "$NAME" --force

docker image rm "$NAME"

docker build -t "$NAME" .

docker run --name "$NAME" -d -p "$PORT":80 "$NAME"
