#+begin_src bash :exports results :results output
files=()

while IFS= read -r -d '' file; do
    files+=("$file")
done < <(find . -name "index.org" -print0)

all_files="|"

for file in "${files[@]}"; do
    file_parent_directory=$(basename "$(dirname "$file")")
    if [[ "$file_parent_directory" == "." ]]; then
        file_parent_directory="Index"
    else
        file_parent_directory="$(tr '[:lower:]' '[:upper:]' <<< ${file_parent_directory:0:1})${file_parent_directory:1}"
    fi
    all_files+=" [[${file}][${file_parent_directory}]] |"
done

echo "${all_files}"
#+end_src

#+RESULTS:
| [[./index.org][Index]] | [[./section/index.org][Section]] |
