FROM nginx:alpine

LABEL maintainer="Roger Gonzalez <roger@rogs.me>"

WORKDIR /app

COPY . .

RUN rm -rf /usr/share/nginx/html/* && \
    apk add --no-cache emacs git && \
    emacs -Q --script build-site.el && \
    cp -R ./public/* /usr/share/nginx/html/ && \
    apk del emacs git && \
    rm -rf * /var/cache/apk/*

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
